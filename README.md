# ICD-10-GM resources from BfArM

This package contains resources for ICD-10-GM, created from the original files from the German Federal Institute for Drugs and Medical Devices (BfArM).
Versions starting 2022 are taken directly from the National Terminology Server (NTS) of the BfArM, available at https://terminologien.bfarm.de

For versions starting 2022, the properties in the generated CodeSystem resources are coded differently to those in the previous versions.
The resources from the BfArM NTS are used as-is, while the resources from the previous versions are generated from the original ClaML files and use different logic for property encoding. 
Those previous versions will not be updated to the new encoding scheme.
