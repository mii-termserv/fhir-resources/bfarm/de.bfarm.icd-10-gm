#!/bin/zsh

jar_file="$(realpath ./babelfsh.jar)"

function babelfsh() {
  java -jar $jar_file $@
}

if [ ! -f $jar_file ]; then 
  echo "Could not find JAR file at $jar_file"
  exit 1
fi

babelfsh convert -i $(realpath .) -o $(realpath package)
